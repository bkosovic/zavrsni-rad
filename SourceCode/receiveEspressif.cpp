// Kod za predajnik na Espressif ESP32 mikrokontroleru

#include <RH_ASK.h>
#ifdef RH_HAVE_HARDWARE_SPI
#include <SPI.h> 
#endif

// Dodjeljivanje GPIO pinova za LED diode
#define yellowLED 26
#define redLED 32
#define greenLED 27

// Inicijalizacija drivera, za prijamnik se koristi pin 4.s
RH_ASK driver(2000, 4, 5, 0); 

// Definicija varijabli potrebnih za spremanje stanja LED dioda
int greenLedState = 0;
int yellowLedState = 0;
int redLedState = 0;

// Funkcija koja se poziva pri pokretanju mikrokontrolera,
// inicijalizira potrebne GPIO pinove te driver za RF modul.
void setup(){
    pinMode(greenLED, OUTPUT);
    digitalWrite(greenLED, LOW);
    digitalWrite(greenLED, HIGH);
    delay(500);
    digitalWrite(greenLED, LOW);

    pinMode(yellowLED, OUTPUT);
    digitalWrite(yellowLED, LOW);
    digitalWrite(yellowLED, HIGH);
    delay(500);
    digitalWrite(yellowLED, LOW);

    pinMode(redLED, OUTPUT);
    digitalWrite(redLED, LOW);
    digitalWrite(redLED, HIGH);
    delay(500);
    digitalWrite(redLED, LOW);

#ifdef RH_HAVE_SERIAL
    Serial.begin(9600);
#endif
    if (!driver.init())
#ifdef RH_HAVE_SERIAL
        Serial.println("init failed");
#else
        ;
#endif
}

// Funkcija koja se beskonačno izvodi, tj. dok se ne ugasi mikrokontroler
void loop(){
    uint8_t buf[RH_ASK_MAX_MESSAGE_LEN];
    uint8_t buflen = sizeof(buf);

    // U sljedećem se bloku provjerava je li zaprimljena naredba na RF prijamniku,
    // te ako je zaprimljna, koja je to naredba i paljenje ili gašenje odgovarajuće LED diode
    if (driver.recv(buf, &buflen)){
        Serial.println(*buf);
        if (*buf == 98){
            if (greenLedState == 0){
                digitalWrite(greenLED, HIGH);
                greenLedState = 1;
                uint8_t buf[RH_ASK_MAX_MESSAGE_LEN];
                uint8_t buflen = sizeof(buf);
            }
            else{
                digitalWrite(greenLED, LOW);
                greenLedState = 0;
                uint8_t buf[RH_ASK_MAX_MESSAGE_LEN];
                uint8_t buflen = sizeof(buf);
            }
        }
        else if (*buf == 99){
            if (yellowLedState == 0){
                digitalWrite(yellowLED, HIGH);
                yellowLedState = 1;
                uint8_t buf[RH_ASK_MAX_MESSAGE_LEN];
                uint8_t buflen = sizeof(buf);
            }
            else{
                digitalWrite(yellowLED, LOW);
                yellowLedState = 0;
                uint8_t buf[RH_ASK_MAX_MESSAGE_LEN];
                uint8_t buflen = sizeof(buf);
            }
        }
        else if (*buf == 100){
            if (redLedState == 0){
                digitalWrite(redLED, HIGH);
                redLedState = 1;
                uint8_t buf[RH_ASK_MAX_MESSAGE_LEN];
                uint8_t buflen = sizeof(buf);
            }
            else{
                digitalWrite(redLED, LOW);
                redLedState = 0;
                uint8_t buf[RH_ASK_MAX_MESSAGE_LEN];
                uint8_t buflen = sizeof(buf);
            }
        }
    }
}